import React from "react";
import Fruta from "./Components/Fruta";

class App extends React.Component {
  render() {
    const fruits = [
      { name: "banana", color: "yellow", price: 2 },
      { name: "cherry", color: "red", price: 3 },
      { name: "strawberry", color: "red", price: 4 },
    ];

    return (
      <div className="App">
        <div id="all fruit names">
          <Fruta fruits={fruits} />
        </div>
        <div id="red fruit names">
          <Fruta fruits={fruits.filter((item) => item.color === "red")} />
        </div>
        <div id="total">
          <Fruta
            fruits={[
              {
                name: fruits.reduce((total, currentValue) => {
                  return total + currentValue.price;
                }, 0),
              },
            ]}
          />
        </div>
      </div>
    );
  }
}

export default App;
